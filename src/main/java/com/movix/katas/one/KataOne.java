package com.movix.katas.one;

import com.movix.katas.one.exception.NoValidMsisdnException;

public class KataOne {

	public String processCommand(String msisdn, String command) throws NoValidMsisdnException {

		boolean aSwitch = false;

		if (msisdn != null && command != null && command.length() > 0) {

			if (msisdn.length() < 11 || msisdn.length() > 11) {
				throw new NoValidMsisdnException();
			} else {
				aSwitch = true;
			}

			//if the command is salir, check for subscription
			if(command.toLowerCase().contains("salir")){
				if(command.toLowerCase().contains("juegos")) {
					return "Te has desuscrito de JUEGOS";
				}else if(command.toLowerCase().contains("horoscopo")){
					return "Te has desuscrito de HOROSCOPO";
				}
			}

			if (command.equalsIgnoreCase("echo") && aSwitch) {
				return command;
			}

			if (command.equalsIgnoreCase("juegos")) {
				return "Bienvenido. Ya te encuentras suscrito a JUEGOS";
			}

			if (command.equalsIgnoreCase("ayuda")) {
				return "Envia \"echo\" para probar el servicio. Envia \"juegos\" para suscribirte a Juegos.";
			}

			if (command.equalsIgnoreCase("horoscopo")) {
				return "Bienvenido. Ya te encuentras suscrito a HOROSCOPO";
			}

			int Command_Length = command.length();
			if ((Command_Length == 9 || Command_Length == 11) && aSwitch) {
				// command could be a msisdn, so we must send a gift
				try {
					Long.parseLong(command);

					// command is a msisdn. we need to send a gift. we need to
					// include the msisdn but in local format
					String msisdn2 = (command.length() == 11) ? command.substring(2) : command;

					return "Has regalado un paquete de datos a tu amig@ " + msisdn2;
				} catch (NumberFormatException e) {
					// nothing to do
				}

			}

			return "Comando desconocido. Envie \nayuda\n para ver los comandos disponibles";

		} else if (msisdn == null) {
			throw new NoValidMsisdnException();
		}

		return null;
	}

}
